"use strict";
var maincode = maincode || {};
maincode.worldcode = function() {

    var that = {},

        lastCalledTime = [],
        frameTimes = [],
        resolutionFactor = 1,
        fps = [],
        longTermRenderFps = [],
        messagesAll = [],
        animationLoop,
        scene,
        camera,
        renderer,
        canvas,
        initTimeout_Intervall_Libary,
        intervall60,
        intervall30,

        intervall_10,
        intervall0_1,
        originalWidth,
        originalHeight,
        asteroidsAll = [],
        enemys = [],
        allObjects = [],

        geometryPlayerShip,
        geometryEnemyShip,
        geometryAsteroid,
        keyworker,
        logic,

        laserMatireal,
        laserMatirealEnemy,

        materialInvisible,

        physicMaterialWorld,
        physicMaterialPlayer,


        laserBeamsLeft = [],
        laserBeamsRight = [],

        laserBeamsLeftEnemy = [],
        laserBeamsRightEnemy = [],

        closesed = Number.MAX_VALUE,
        closesedEnemy = Number.MAX_VALUE,
        closesdObj,
        closesdObjEnemy,
        weaponRange = 1750,
        enemyWeaponRange = 2000,
        zSpeed = 0,
        xSpeed = 0,
        maxSpeed = 100,
        accelerationSide = 24,
        accelerationBack = 24,
        accelerationForward = 36,
        accelerationForwardEnemy = 36,
        turnSpeedEnemy = 30,
        turnSpeed = 40,

        credits = 0,
        pause = false,
        playerShipMesh,
        shipObj,
        loader,

        frictionPlayer = 1.0,
        frictionWorld = 1.0,
        frictionEnemyShips = 0.25,
        frictionAsteroids = 0.1,

        restitution = 0.3,

        playerHull = 100,
        playerShield = 100,

        asteroidMass = 3,
        enemyMass = 2,
        playerMass = 1,
        laserDmgPlayer = 0.1,
        laserDmgEnemy = 0.15,
        asteroidObj,
        fpsLimit = 30,
        color,
        maxNumberOfShootingEnemys = 10,
        maxNumberOfAsteroids = 50,

        soundID = "Explosion",

        explosionNumber = 10,
        explosionNumberCurrent = 0,
        explosionGroup = [explosionNumber],
        mapComposer,
        composer,
        mapCamera,
        buttonWorker,
        savedTargetFps;



    function init(keys, logik, bW) {
        buttonWorker = bW;
        /* eslint-disable */
        createjs.Sound.registerSound("src/Sound/Grenade Explosion-SoundBible.com-2100581469.mp3", soundID, 10);
        /* eslint-enable */
        setFpsCounters(5);
        keyworker = keys;
        logic = logik;


        credits = logic.getCredits();
        maxSpeed = logic.getMaxSpeed();
        weaponRange = logic.getWeaponRange();

        playerHull = logic.getHull();
        playerShield = logic.getShield();

        laserDmgPlayer = logic.getDamage();
        accelerationBack = accelerationBack + logic.getAcc();
        accelerationForward = accelerationForward + logic.getAcc();

        accelerationSide = accelerationSide + logic.getAcc();


        for (let index = 0; index < maxNumberOfShootingEnemys; index++) {
            laserBeamsLeftEnemy[index] = [];
            laserBeamsRightEnemy[index] = [];
        }



        /* eslint-disable */
        Physijs.scripts.worker = "src/Libary/physijs_worker.js";
        Physijs.scripts.ammo = "ammo.js";

        scene = new Physijs.Scene({
            reportsize: 5,
            fixedTimeStep: 1 / 60
        });

        scene.setGravity(new THREE.Vector3(0, -90, 0));
        /* eslint-enable */
        scene.simulate(undefined, 1);

        /*
                loader = new THREE.ObjectLoader();


                loader.load(

                    "src/Objects/station/space-station.json",

                    function(object) {
                  //could use stationModel if needed

                    }
                );
        */
        /* eslint-disable */
        loader = new THREE.OBJMTLLoader();
        /* eslint-enable */
        loader.load(
            "src/Objects/Sun_Glyder.obj", "src/Objects/Sun_Glyder.mtl",
            function(object) {
                playerShipMesh.add(object);
                object.rotation.y -= Math.PI;
                object.position.set(0, -44, -44);
                object.scale.set(0.5, 0.5, 0.5);
                shipObj = object;
                scene.add(playerShipMesh);
                playerShipMesh.__dirtyPosition = true;

                respawnAstro(playerShipMesh);
                createAllEnemyShips(maxNumberOfShootingEnemys);


            },
            function() {},
            function() {});

        loader.load(
            "src/Objects/astro/asteroid.obj", "src/Objects/astro/asteroid.mtl",
            function(object) {
                asteroidObj = object;
                createAllAsteroids(maxNumberOfAsteroids);


            },
            function() {},
            function() {});

        /* eslint-disable */
        geometryPlayerShip = new THREE.BoxGeometry(125, 60, 250);
        geometryAsteroid = new THREE.BoxGeometry(125, 60, 250);
        geometryEnemyShip = new THREE.BoxGeometry(125, 60, 250);

        color = new THREE.Color("rgb(92%, 92%,100%)");
        scene.add(new THREE.AmbientLight(color));

        color = new THREE.Color("rgb(90%, 90%, 90%)");
        materialInvisible = new THREE.MeshBasicMaterial({
            color: color,
            transparent: true,
            opacity: 0,
            wireframe: true
        });

        color = new THREE.Color("rgb(100%, 0%, 0%)");
        laserMatireal = new THREE.MeshPhongMaterial({
            color: color,
            emissive: color,
            specular: color,
            shininess: 900,
            shading: THREE.SmoothShading
        });

        color = new THREE.Color("rgb(0%, 100%, 0%)");
        laserMatirealEnemy = new THREE.MeshPhongMaterial({
            color: color,
            emissive: color,
            specular: color,
            shininess: 900,
            shading: THREE.SmoothShading
        });

        physicMaterialWorld = Physijs.createMaterial(
            materialInvisible,
            frictionWorld,
            restitution
        );

        physicMaterialPlayer = Physijs.createMaterial(
            materialInvisible,
            frictionPlayer,
            restitution
        );

        playerShipMesh = new Physijs.BoxMesh(geometryPlayerShip, physicMaterialPlayer, playerMass);

        /* eslint-enable */
        playerShipMesh.userData.name = "Player";
        playerShipMesh.userData.live = playerHull;
        playerShipMesh.userData.MaxLive = playerHull;
        playerShipMesh.userData.MaxShield = playerShield;
        playerShipMesh.userData.shield = playerShield;
        playerShipMesh.userData.shootReadyL = 0;
        playerShipMesh.userData.shootReadyR = 0;
        playerShipMesh.userData.alive = true;








        createWalls();
        /* eslint-disable */
        renderer = new THREE.WebGLRenderer({
            antialias: true,
            precision: "highp",
            logarithmicDepthBuffer: false
        });
        /* eslint-enable */

        renderer.setFaceCulling(1);
        renderer.setSize(window.innerWidth, window.innerHeight);

        document.body.appendChild(renderer.domElement);
        canvas = renderer.domElement;
        originalWidth = canvas.width;
        originalHeight = canvas.height;
        /* eslint-disable */
        camera = new THREE.TargetCamera(75, window.innerWidth / window.innerHeight, 50, 55000);
        /* eslint-enable */
        camera.addTarget({
            name: "myTarget",
            targetObject: playerShipMesh,
            /* eslint-disable */
            cameraPosition: new THREE.Vector3(0, 40, 200),
            /* eslint-enable */
            fixed: false,
            stiffness: 0.97,
            matchRotation: true
        });
        camera.addTarget({
            name: "myTargetDead",
            targetObject: playerShipMesh,
            /* eslint-disable */
            cameraPosition: new THREE.Vector3(0, 100, 500),
            /* eslint-enable */
            fixed: false,
            stiffness: 0.1,
            matchRotation: true
        });
        camera.setTarget("myTarget");



        initTimeout_Intervall_Libary = setTimeout(function() {}, 1);

        intervall60 = setInterval(update60, (1 / 60) * 1000);

        intervall30 = setInterval(update30, (1 / 30) * 1000);
        intervall_10 = setInterval(update10, (1 / 10) * 1000);
        intervall0_1 = setInterval(update01, (10 / 1) * 1000);
        /* eslint-disable */
        animationLoop = limitLoop(smoothFpsAnimate, fpsLimit);
        /* eslint-enable */
        animationLoop.loop();

        playerShipMesh.addEventListener("collision", handleCollision);
        scene.addEventListener("update", updatedPhysics);
        window.addEventListener("resize", onWindowResize, false);
        window.addEventListener("changeFpsTarget", changeCurrentFps, false);



        pauseGame();
        /* eslint-disable */
        THREE.DefaultLoadingManager.onProgress = function(item, loaded, total) {
            /* eslint-enable */
            if (loaded == total) {

                modelsLoaded();
            }

        };






        /* eslint-disable */
        mapCamera = new THREE.OrthographicCamera(-3000,
            /* eslint-enable */
            3000,
            3000, -3000,
            0.1,
            1200);
        /* eslint-disable */
        mapCamera.up = new THREE.Vector3(0, 0, -1);
        mapCamera.lookAt(new THREE.Vector3(0, -1, 0));
        /* eslint-enable */
        mapCamera.position.y = 1000;



        scene.add(mapCamera);

        initParticles();
        initImageProcessing();
        
        
        


    }

    function initImageProcessing() {
        renderer.autoClear = false;

        renderer.setClearColor("#000000");

        /* eslint-disable */
        composer = new THREE.EffectComposer(renderer);
        var renderModel = new THREE.RenderPass(scene, camera),
            effectCopy = new THREE.ShaderPass(THREE.CopyShader),
            /* eslint-enable */
            renderModelMiniMap, effectCopy2, effectFXAA2;
        effectCopy.renderToScreen = true;

        composer.addPass(renderModel);

        composer.addPass(effectCopy);
        composer.setSize(window.innerWidth * 2, window.innerHeight * 2);
        /* eslint-disable */
        mapComposer = new THREE.EffectComposer(renderer, new THREE.WebGLRenderTarget(512, 512));
        /* eslint-enable */
        mapComposer.setSize(512, 512);
        /* eslint-disable */
        renderModelMiniMap = new THREE.RenderPass(scene, mapCamera);
        /* eslint-enable */
        mapComposer.addPass(renderModelMiniMap);
        /* eslint-disable */
        effectFXAA2 = new THREE.ShaderPass(THREE.FXAAShader);
        /* eslint-enable */
        effectFXAA2.uniforms["resolution"].value.set(1 / 512, 1 / 512);

        mapComposer.addPass(effectFXAA2);
        /* eslint-disable */
        effectCopy2 = new THREE.ShaderPass(THREE.CopyShader);
        /* eslint-enable */
        effectCopy2.renderToScreen = true;
        mapComposer.addPass(effectCopy2);
    }

    function changeCurrentFps(event) {

        savedTargetFps = event.detail;
        changeFps(event.detail);

    }

    function setResolution(scale) {
        resolutionFactor = scale;


        canvas.width = Math.round(originalWidth * scale);
        canvas.height = Math.round(originalHeight * scale);
        camera.aspect = canvas.width / canvas.height;
        camera.updateProjectionMatrix();
        renderer.setSize(canvas.width, canvas.height);
        composer.setSize(canvas.width * 2, canvas.height * 2);

        canvas.style.width = originalWidth + "px";
        canvas.style.height = originalHeight + "px";

    }

    function onWindowResize() {
        originalWidth = window.innerWidth;
        originalHeight = window.innerHeight;

        setResolution(resolutionFactor);



    }

    function initParticles() {


        /* eslint-disable */
        var fireball = new SPE.Emitter({
            /* eslint-enable */
            particleCount: 30000,
            duration: 0.5,
            /* eslint-disable */
            type: SPE.distributions.SPHERE,
            /* eslint-enable */
            position: {
                radius: 250
            },
            maxAge: {
                value: 1
            },

            activeMultiplier: 20,
            velocity: {
                /* eslint-disable */
                value: new THREE.Vector3(200)
                    /* eslint-enable */
            },
            acceleration: {
                /* eslint-disable */
                value: new THREE.Vector3(100)
                    /* eslint-enable */

            },

            randomise: {
                value: true
            },
            size: {
                value: [10, 70]
            },
            color: {
                value: [
                    /* eslint-disable */
                    new THREE.Color(0.5, 0.1, 0.05),
                    new THREE.Color(0.2, 0.2, 0.2)
                    /* eslint-enable */
                ]
            },
            alive: true,
            opacity: {
                value: [0.5, 0.35, 0.1, 0]
            }
        });
        /* eslint-disable */
        var loader = new THREE.TextureLoader();
        /* eslint-enable */
        loader.load(
            "src/Images/sprite-explosion2.png",

            function(textur) {

                for (let index = 0; index < explosionNumber; index++) {
                    /* eslint-disable */
                    explosionGroup[index] = new SPE.Group({
                        /* eslint-enable */
                        texture: {

                            value: textur,
                            /* eslint-disable */
                            frames: new THREE.Vector2(5, 5),
                            /* eslint-enable */
                            loop: 1
                        },
                        depthTest: true,
                        depthWrite: false,
                        /* eslint-disable */
                        blending: THREE.AdditiveBlending,
                        /* eslint-enable */
                        scale: 600
                    });

                    explosionGroup[index].addPool(1, fireball, true);
                    scene.add(explosionGroup[index].mesh);
                }

            },
            function() {},
            function() {});









    }





    function modelsLoaded() {
        buttonWorker.readyLoaded();
        getNearestObj();


    }

    function changeFps(fpsTarget) {
        fpsLimit = fpsTarget;
        fpsCurrentTargetEvent(fpsLimit);

        animationLoop.changeFps(fpsLimit);

    }



    function enableWireframeAsteroids(visible) {
        var index;
        for (index = 0; index < asteroidsAll.length; index++) {
            if (visible) {
                asteroidsAll[index].material.opacity = 1;
            } else {
                asteroidsAll[index].material.opacity = 0;
            }

        }
        if (asteroidsAll[asteroidsAll.length - 1] !== undefined) {
            return true;
        }
        return false;
    }

    function enableWireframeEnemys(visible) {
        var index;
        for (index = 0; index < enemys.length; index++) {
            if (visible) {
                enemys[index].material.opacity = 1;
            } else {
                enemys[index].material.opacity = 0;
            }

        }
        if (enemys[enemys.length - 1] !== undefined) {
            return true;
        }
        return false;
    }

    function createEnemy(x, y, z, live, shield, worth, shieldReg) {
        /* eslint-disable */
        var meshEnemy, color = new THREE.Color("rgb(0%, 0%, 0%)"),
            materialEnemyShip = new THREE.MeshBasicMaterial({
                /* eslint-enable */
                color: color,
                transparent: true,
                opacity: 0,
                wireframe: true
            }),
            enemyShip;
        /* eslint-disable */
        meshEnemy = new Physijs.BoxMesh(geometryEnemyShip, Physijs.createMaterial(
            /* eslint-enable */
            materialEnemyShip,
            frictionEnemyShips,
            restitution
        ), enemyMass);

        meshEnemy.__dirtyPosition = true;
        meshEnemy.position.set(x, y, z);

        scene.add(meshEnemy);

        meshEnemy.userData.live = live;
        meshEnemy.userData.MaxLive = live;
        meshEnemy.userData.MaxShield = shield;
        meshEnemy.userData.shield = shield;
        meshEnemy.userData.shieldReg = shieldReg;
        meshEnemy.userData.name = "Enemy Ship";
        meshEnemy.userData.worth = worth;
        meshEnemy.userData.mass = enemyMass;
        meshEnemy.userData.alive = true;
        meshEnemy.userData.enemy = true;
        meshEnemy.userData.shootReadyL = 0;
        meshEnemy.userData.shootReadyR = 0;
        meshEnemy.userData.engineforward = 0;

        enemyShip = shipObj.clone();
        meshEnemy.add(enemyShip);

        enemys.push(meshEnemy);
        allObjects.push(meshEnemy);
        return meshEnemy;

    }

    function createWalls() {
        /* eslint-disable */
        var loader = new THREE.TextureLoader(),
            mesh, meshUp, meshF, meshB, meshL, meshR, material, geometry = new THREE.BoxGeometry(50000, 1, 50000),
            geometryFront = new THREE.BoxGeometry(20000, 2000, 1),
            geometrySide = new THREE.BoxGeometry(1, 2000, 20000);
        /* eslint-enable */
        loader.load(
            "src/Images/space12.jpg",

            function(texture) {
                var skyGeo, sky;
                /* eslint-disable */
                texture.wrapS = texture.wrapT = THREE.MirroredRepeatWrapping;
                /* eslint-enable */
                texture.repeat.set(4, 1);
                /* eslint-disable */
                material = new THREE.MeshPhongMaterial({
                    /* eslint-enable */
                    map: texture
                });

                /* eslint-disable */
                skyGeo = new THREE.SphereGeometry(40000, 25, 25);
                sky = new THREE.Mesh(skyGeo, material);
                sky.material.side = THREE.BackSide;
                /* eslint-enable */
                scene.add(sky);

            },
            function() {},
            function() {});




        /* eslint-disable */
        mesh = new Physijs.BoxMesh(geometry, physicMaterialWorld, 0);
        meshUp = new Physijs.BoxMesh(geometry, physicMaterialWorld, 0);
        meshF = new Physijs.BoxMesh(geometryFront, physicMaterialWorld, 0);
        meshB = new Physijs.BoxMesh(geometryFront, physicMaterialWorld, 0);

        meshL = new Physijs.BoxMesh(geometrySide, physicMaterialWorld, 0);
        meshR = new Physijs.BoxMesh(geometrySide, physicMaterialWorld, 0);
        /* eslint-enable */
        meshF.__dirtyPosition = true;

        meshF.position.set(0, 0, -10000);

        meshUp.__dirtyPosition = true;
        meshUp.position.set(0, 2000, 0);

        meshB.__dirtyPosition = true;
        meshB.position.set(0, 0, 10000);

        meshL.__dirtyPosition = true;
        meshL.position.set(-10000, 0, 0);

        meshR.__dirtyPosition = true;
        meshR.position.set(10000, 0, 0);

        scene.add(meshR);
        scene.add(meshUp);
        scene.add(meshL);
        scene.add(meshF);
        scene.add(meshB);





        mesh.position.set(0, -200, 0);
        mesh.__dirtyPosition = true;
        scene.add(mesh);

    }

    function createAllEnemyShips(number) {



        for (let index = 0; index < number; index++) {

            respawnAstro(createEnemy(0, 0, 0, 100 + Math.round(Math.random() * 10) * 10, Math.round(Math.random() * 10) * 10, Math.round(Math.random() * 100), 10));
        }
    }

    function createAllAsteroids(number) {

        for (let index = 0; index < number; index++) {

            respawnAstro(createAsteroid(0, 0, 0, Math.round(Math.random() * 120 + 70), 0, Math.round(Math.random() * 100)));
        }

    }

    function handleCollision(other_object, relative_velocity) {
        var colDmg, plyDmg, otherDmg;
        if (typeof other_object.userData.name != "undefined") {
            colDmg = (((relative_velocity.x * relative_velocity.x) + (relative_velocity.z * relative_velocity.z)) / 100);
            plyDmg = Math.round((1 - (playerMass / (playerMass + other_object.userData.mass))) * (colDmg));
            otherDmg = Math.round((1 - (other_object.userData.mass / (playerMass + other_object.userData.mass))) * (colDmg));


            doNPCNPCDmg(otherDmg, other_object, true);

            doNPCNPCDmg(plyDmg, playerShipMesh, false);
        }


    }



    function doNPCNPCDmg(dmg, obj, damageFromPlayer) {
        var position;
        if (obj.userData.shield > 0) {
            obj.userData.shield = obj.userData.shield - dmg;
            if (obj.userData.shield < 0) {
                obj.userData.shield = 0;
            }

        } else {

            if (obj.userData.live > 0) {
                obj.userData.live = obj.userData.live - dmg;

                obj.material.color.setRGB((obj.userData.MaxLive - obj.userData.live) / obj.userData.MaxLive, 0.0, 0.0);

                if (obj.userData.live < 0) {
                    obj.userData.live = 0;
                }
            }
        }
        if (obj.userData.live == 0) {
            if (obj.userData.alive) {
                obj.userData.alive = false;
                if (obj.userData.name != "Player") {


                    if (damageFromPlayer) {
                        credits = credits + obj.userData.worth;
                        logic.safeCredits(obj.userData.worth);
                        pushToMessageBox(obj.userData.name + " was destroyed gave " + obj.userData.worth + " Credits");
                    } else {
                        pushToMessageBox(obj.userData.name + " was destroyed by Enemy Ship");
                    }
                    /* eslint-disable */
                    position = new THREE.Vector3();
                    /* eslint-enable */
                    position.setFromMatrixPosition(obj.matrixWorld);



                    setTimeout(showExplosion.bind(null, position), 10);

                    respawnAstro(obj);

                } else {

                    playerShipMesh.userData.alive = false;

                    setTimeout(playerDeath, 5000);
                    camera.setTarget("myTargetDead");
                    setTimeout(function() {
                        pauseGame();

                        dispatchNewEvent("showDeadScreen", "");

                    }, 6300);

                }
            }

        }
    }

    function dispatchNewEvent(eventName, detail) {
        var event = new CustomEvent(eventName, {
            "detail": detail
        });
        window.dispatchEvent(event);


    }

    function makeRestartPossible() {
        playerShipMesh.userData.live = playerShipMesh.userData.MaxLive;
        playerShipMesh.userData.shield = playerShipMesh.userData.MaxShield;
        playerShipMesh.userData.alive = true;
        camera.setTarget("myTarget");
        respawnAstro(playerShipMesh);



    }

    function playerDeath() {
        /* eslint-disable */
        var position = new THREE.Vector3();
        /* eslint-enable */
        playerShipMesh.visible = false;
        position.setFromMatrixPosition(playerShipMesh.matrixWorld);
        showExplosion(position);
    }

    function convertRange(value, r1, r2) {
        return (value - r1[0]) * (r2[1] - r2[0]) / (r1[1] - r1[0]) + r2[0];
    }

    function showExplosion(position) {
        /* eslint-disable */
        var playerPosition = new THREE.Vector3(),
            /* eslint-enable */
            difference, volume;

        playerPosition.setFromMatrixPosition(playerShipMesh.matrixWorld);

        difference = Math.sqrt(((playerPosition.x - position.x) * (playerPosition.x - position.x)) + ((playerPosition.z - position.z) * (playerPosition.z - position.z)));
        volume = 0;
        if (difference < 5000) {
            volume = 1 - (convertRange(difference, [0, 5000], [0, 1]));
        }

        /* eslint-disable */
        createjs.Sound.play(soundID, {
            /* eslint-enable */
            volume: volume
        });


        explosionGroup[explosionNumberCurrent].mesh.position.set(position.x, position.y, position.z);
        explosionGroup[explosionNumberCurrent].triggerPoolEmitter(1);
        if (explosionNumberCurrent < explosionNumber - 1) {
            explosionNumberCurrent = explosionNumberCurrent + 1;
        } else {
            explosionNumberCurrent = 0;
        }



    }

    function createAsteroid(x, y, z, live, shield, worth) {
        /* eslint-disable */
        var color = new THREE.Color("rgb(0%, 0%, 0%)"),
            asteroidObjcur, meshAsteroid, materialAsteroid = new THREE.MeshBasicMaterial({
                /* eslint-enable */
                color: color,
                transparent: true,
                opacity: 0,
                wireframe: true
            });
        /* eslint-disable */
        meshAsteroid = new Physijs.BoxMesh(geometryAsteroid, Physijs.createMaterial(
            /* eslint-enable */
            materialAsteroid,
            frictionAsteroids,
            restitution
        ), asteroidMass);

        meshAsteroid.__dirtyPosition = true;
        meshAsteroid.position.set(x, y + 100, z);
        meshAsteroid.scale.set(5, 10, 5);
        scene.add(meshAsteroid);
        meshAsteroid.__dirtyPosition = false;

        meshAsteroid.userData.live = live;
        meshAsteroid.userData.MaxLive = live;
        meshAsteroid.userData.MaxShield = shield;
        meshAsteroid.userData.shield = shield;
        meshAsteroid.userData.name = "Asteroid";
        meshAsteroid.userData.worth = worth;
        meshAsteroid.userData.mass = asteroidMass;
        meshAsteroid.userData.alive = true;
        meshAsteroid.userData.enemy = false;

        asteroidObjcur = asteroidObj.clone();
        asteroidObjcur.scale.set(0.28, 0.16, 0.6);
        meshAsteroid.add(asteroidObjcur);
        asteroidObjcur.position.set(0, -25, -10);

        asteroidsAll.push(meshAsteroid);
        allObjects.push(meshAsteroid);
        return meshAsteroid;
    }



    function updatedPhysics() {
        var position;
        if (!pause) {
            xSpeed = playerShipMesh.getLinearVelocity().x;
            zSpeed = playerShipMesh.getLinearVelocity().z;
            /* eslint-disable */
            position = new THREE.Vector3();
            /* eslint-enable */
            position.setFromMatrixPosition(playerShipMesh.matrixWorld);
            mapCamera.position.z = position.z;
            mapCamera.position.x = position.x;
            if (playerShipMesh.userData.alive) {
                keyworker.moveIt();
            }
            enemyMovementlogic();
        }
    }

    function turnLeft() {
        /* eslint-disable */
        var rotation_matrix = new THREE.Matrix4().extractRotation(playerShipMesh.matrix);
        /* eslint-enable */
        playerShipMesh.applyForce(
            /* eslint-disable */
            new THREE.Vector3(0, 0, 40).applyMatrix4(rotation_matrix),
            new THREE.Vector3((-1) * turnSpeed, 0, 0).applyMatrix4(rotation_matrix));
        /* eslint-enable */
        playerShipMesh.applyForce(
            /* eslint-disable */
            new THREE.Vector3(0, 0, -40).applyMatrix4(rotation_matrix),
            new THREE.Vector3(turnSpeed, 0, 0).applyMatrix4(rotation_matrix));
        /* eslint-enable */
    }

    function turnRight() {
        /* eslint-disable */
        var rotation_matrix = new THREE.Matrix4().extractRotation(playerShipMesh.matrix);
        /* eslint-enable */
        playerShipMesh.applyForce(
            /* eslint-disable */
            new THREE.Vector3(0, 0, 40).applyMatrix4(rotation_matrix),
            new THREE.Vector3(turnSpeed, 0, 0).applyMatrix4(rotation_matrix));
        /* eslint-enable */
        playerShipMesh.applyForce(
            /* eslint-disable */
            new THREE.Vector3(0, 0, -40).applyMatrix4(rotation_matrix),
            new THREE.Vector3((-1) * turnSpeed, 0, 0).applyMatrix4(rotation_matrix));
        /* eslint-enable */

    }

    function moveforward() {
        var rotation_matrix, force_vector;

        if (getSpeed() < maxSpeed) {
            /* eslint-disable */
            rotation_matrix = new THREE.Matrix4().extractRotation(playerShipMesh.matrix);
            force_vector = new THREE.Vector3(0, 0, (-1) * accelerationForward).applyMatrix4(rotation_matrix);
            /* eslint-enable */
            playerShipMesh.applyCentralForce(force_vector);
        }


    }

    function clearIntervalls() {
        clearInterval(intervall60);
        clearInterval(intervall30);
        clearInterval(intervall0_1);
        clearInterval(intervall_10);
        clearInterval(initTimeout_Intervall_Libary);


    }

    function movebackward() {
        var rotation_matrix, force_vector;
        if (getSpeed() < maxSpeed) {
            /* eslint-disable */
            rotation_matrix = new THREE.Matrix4().extractRotation(playerShipMesh.matrix);
            force_vector = new THREE.Vector3(0, 0, accelerationBack).applyMatrix4(rotation_matrix);
            /* eslint-enable */
            playerShipMesh.applyCentralForce(force_vector);
        }

    }

    function moveleft() {
        var rotation_matrix, force_vector;
        if (getSpeed() < maxSpeed) {
            /* eslint-disable */
            rotation_matrix = new THREE.Matrix4().extractRotation(playerShipMesh.matrix);
            force_vector = new THREE.Vector3((-1) * accelerationSide, 0, 0).applyMatrix4(rotation_matrix);
            /* eslint-enable */
            playerShipMesh.applyCentralForce(force_vector);
        }

    }

    function moveright() {
        var rotation_matrix, force_vector;
        if (getSpeed() < maxSpeed) {
            /* eslint-disable */
            rotation_matrix = new THREE.Matrix4().extractRotation(playerShipMesh.matrix);
            force_vector = new THREE.Vector3(accelerationSide, 0, 0).applyMatrix4(rotation_matrix);
            /* eslint-enable */
            playerShipMesh.applyCentralForce(force_vector);
        }

    }

    function getSpeed() {
        var speed;
        var pythagoras = Math.pow(xSpeed, 2) + Math.pow(zSpeed, 2);
        speed = Math.sqrt(pythagoras);

        return speed;
    }

    function getNearestObj() {
        var otherx, otherz, difference, position, ownx, ownz;



        closesed = Number.MAX_VALUE;

        closesedEnemy = Number.MAX_VALUE;


        /* eslint-disable */
        position = new THREE.Vector3();
        /* eslint-enable */
        position.setFromMatrixPosition(playerShipMesh.matrixWorld);
        ownx = position.x;



        ownz = position.z;
        for (let index = 0; index < allObjects.length; ++index) {

            /* eslint-disable */
            position = new THREE.Vector3();
            /* eslint-enable */
            position.setFromMatrixPosition(allObjects[index].matrixWorld);
            otherx = position.x;
            otherz = position.z;

            difference = Math.sqrt(((ownx - otherx) * (ownx - otherx)) + ((ownz - otherz) * (ownz - otherz)));
            if (allObjects[index].userData.enemy == true) {
                if (difference < closesedEnemy) {
                    closesedEnemy = difference;
                    closesdObjEnemy = allObjects[index];

                }
            }
            if (difference < closesed) {
                closesed = difference;
                closesdObj = allObjects[index];

            }

        }
    }

    function showShootRight(obj) {
        var pointX, pointY, material, rotation_matrix, direction, orientation, edgeGeometry, edge;
        if (!laserBeamsRight.length) {
            /* eslint-disable */
            rotation_matrix = new THREE.Matrix4().extractRotation(playerShipMesh.matrix);
            /* eslint-enable */




            /* eslint-disable */
            pointX = playerShipMesh.position.clone().add((new THREE.Vector3(45, -5.0, -20.0)).applyMatrix4(rotation_matrix));
            /* eslint-enable */

            pointY = obj.position;


            material = laserMatireal;
            /* eslint-disable */
            direction = new THREE.Vector3().subVectors(pointY, pointX);
            orientation = new THREE.Matrix4();
            orientation.lookAt(pointX, pointY, new THREE.Object3D().up);
            orientation.multiply(new THREE.Matrix4().set(1, 0, 0, 0,
                /* eslint-enable */
                0, 0, 1, 0,
                0, -1, 0, 0,
                0, 0, 0, 1));
            /* eslint-disable */
            edgeGeometry = new THREE.CylinderGeometry(0.9, 0.9, direction.length(), 64, 1);
            edge = new THREE.Mesh(edgeGeometry, material);
            /* eslint-enable */
            edge.applyMatrix(orientation);

            edge.position.x = (pointY.x + pointX.x) / 2;
            edge.position.y = (pointY.y + pointX.y) / 2;
            edge.position.z = (pointY.z + pointX.z) / 2;




            laserBeamsRight.push(edge);
            scene.add(edge);
        }





    }

    function showShootLeft(obj) {
        var pointX, pointY, material, rotation_matrix, direction, orientation, edgeGeometry, edge;
        if (!laserBeamsLeft.length) {
            /* eslint-disable */
            rotation_matrix = new THREE.Matrix4().extractRotation(playerShipMesh.matrix);
            pointX = playerShipMesh.position.clone().add((new THREE.Vector3(-45, -5.0, -20.0)).applyMatrix4(rotation_matrix));
            /* eslint-enable */

            pointY = obj.position;


            material = laserMatireal;
            /* eslint-disable */
            direction = new THREE.Vector3().subVectors(pointY, pointX);
            orientation = new THREE.Matrix4();
            orientation.lookAt(pointX, pointY, new THREE.Object3D().up);
            orientation.multiply(new THREE.Matrix4().set(1, 0, 0, 0,
                /* eslint-enable */
                0, 0, 1, 0,
                0, -1, 0, 0,
                0, 0, 0, 1));
            /* eslint-disable */
            edgeGeometry = new THREE.CylinderGeometry(0.9, 0.9, direction.length(), 64, 1);
            edge = new THREE.Mesh(edgeGeometry, material);
            /* eslint-enable */
            edge.applyMatrix(orientation);
            edge.position.x = (pointY.x + pointX.x) / 2;
            edge.position.y = (pointY.y + pointX.y) / 2;
            edge.position.z = (pointY.z + pointX.z) / 2;



            laserBeamsLeft.push(edge);
            scene.add(edge);

        }




    }

    function checkIfCollide(x, z) {
        var xDiff, zDiff, diff, objMinDif = 1500;
        for (let index = 0; index < allObjects.length; index++) {

            if (allObjects[index].position.x > x) {
                xDiff = allObjects[index].position.x - x;
            } else {
                xDiff = x - allObjects[index].position.x;

            }

            if (allObjects[index].position.z > z) {
                zDiff = allObjects[index].position.z - z;
            } else {
                zDiff = z - allObjects[index].position.z;

            }
            diff = (zDiff * zDiff) + (xDiff * xDiff);
            if (diff != 0) {
                diff = Math.sqrt(diff);

            }
            if (diff < objMinDif) {
                return true;
            }


        }

        if (playerShipMesh.position.x > x) {
            xDiff = playerShipMesh.position.x - x;
        } else {
            xDiff = x - playerShipMesh.position.x;

        }

        if (playerShipMesh.position.z > z) {
            zDiff = playerShipMesh.position.z - z;
        } else {
            zDiff = z - playerShipMesh.position.z;

        }
        diff = (zDiff * zDiff) + (xDiff * xDiff);
        if (diff != 0) {
            diff = Math.sqrt(diff);

        }
        if (diff < 2000) {
            return true;
        }





        return false;

    }

    function getNewX() {
        var x = 0;
        x = Math.floor(Math.random() * 9000);
        if (Math.random() > 0.5) {
            x = x * (-1);
        }



        return x;
    }

    function getNewZ() {
        var z = 0;
        z = Math.floor(Math.random() * 9000);
        if (Math.random() > 0.5) {
            z = z * (-1);
        }
        return z;
    }

    function respawnAstro(obj) {

        var x = getNewX(),
            z = getNewZ();

        if (checkIfCollide(x, z)) {

            respawnAstro(obj);
        } else {


            obj.visible = false;
            obj.__dirtyPosition = true;
            obj.material.color.setRGB((obj.userData.MaxLive - obj.userData.MaxLive) / 100, 0.0, 0.0);

            if (obj.userData.name == "Player") {
                obj.position.set(x, -170, z);
            } else {
                if (obj.userData.name == "Enemy Ship") {
                    obj.position.set(x, -5, z);
                } else {


                    obj.position.set(x, 100, z);

                }
            }



            setTimeout(showAstro.bind(null, obj), 1500);

        }





    }

    function showAstro(obj) {

        obj.visible = true;
        obj.userData.alive = true;
        obj.userData.live = obj.userData.MaxLive;


    }

    function shootingUpdate() {


        if (typeof closesdObj != "undefined") {




            for (let index = 0; index < laserBeamsLeftEnemy.length; index++) {


                if (laserBeamsLeftEnemy[index].length) {
                    for (let i = 0; i < laserBeamsLeftEnemy[index].length; i++) {
                        scene.remove(laserBeamsLeftEnemy[index][i]);
                    }
                    laserBeamsLeftEnemy[index] = [];

                }


                if (laserBeamsRightEnemy[index].length) {
                    for (let i = 0; i < laserBeamsRightEnemy[index].length; i++) {
                        scene.remove(laserBeamsRightEnemy[index][i]);
                    }
                    laserBeamsRightEnemy[index] = [];

                }


            }




            if (laserBeamsLeft.length) {
                for (let i = 0; i < laserBeamsLeft.length; i++) {
                    scene.remove(laserBeamsLeft[i]);
                }
                laserBeamsLeft = [];

            }
            if (laserBeamsRight.length) {

                for (let i = 0; i < laserBeamsRight.length; i++) {
                    scene.remove(laserBeamsRight[i]);
                }
                laserBeamsRight = [];
            }







        }


    }

    function manuellShooting() {
        var rotation_matrix, leftDmg, rightDmg, casterL, casterR, collisionsL, collisionsR;



        if (playerShipMesh.userData.alive) {
            if (typeof closesdObj != "undefined") {

                if (closesed < weaponRange) {


                    /* eslint-disable */
                    rotation_matrix = new THREE.Matrix4().extractRotation(playerShipMesh.matrix);
                    leftDmg = playerShipMesh.position.clone().add((new THREE.Vector3(-45, 0, -20.0)).applyMatrix4(rotation_matrix));
                    rightDmg = playerShipMesh.position.clone().add((new THREE.Vector3(45, 0, -20.0)).applyMatrix4(rotation_matrix));
                    casterL = new THREE.Raycaster();
                    casterR = new THREE.Raycaster();
                    /* eslint-enable */

                    if (closesedEnemy < weaponRange) {
                        casterL.set(leftDmg, closesdObjEnemy.position.clone().sub(leftDmg).normalize());
                        casterR.set(rightDmg, closesdObjEnemy.position.clone().sub(rightDmg).normalize());
                    } else {
                        casterL.set(leftDmg, closesdObj.position.clone().sub(leftDmg).normalize());
                        casterR.set(rightDmg, closesdObj.position.clone().sub(rightDmg).normalize());
                    }


                    collisionsL = casterL.intersectObjects(allObjects);
                    collisionsR = casterR.intersectObjects(allObjects);

                    if (collisionsL.length)

                    {



                        doNPCNPCDmg(laserDmgPlayer, collisionsL[0].object, true);

                        if (playerShipMesh.userData.shootReadyL == 0) {
                            showShootLeft(collisionsL[0].object);
                            playerShipMesh.userData.shootReadyL = 1;
                        } else {
                            playerShipMesh.userData.shootReadyL = playerShipMesh.userData.shootReadyL - 1;
                        }









                    }
                    if (collisionsR.length)

                    {



                        doNPCNPCDmg(laserDmgPlayer, collisionsR[0].object, true);
                        if (playerShipMesh.userData.shootReadyR == 0) {
                            showShootRight(collisionsR[0].object);
                            playerShipMesh.userData.shootReadyR = 1;
                        } else {
                            playerShipMesh.userData.shootReadyR = playerShipMesh.userData.shootReadyR - 1;
                        }




                    }
                }
            }
        }
    }





    function enemyMovementlogic() {
        /* eslint-disable */
        var position = new THREE.Vector3(),
            /* eslint-enable */
            ownx, ownz, index, otherx, otherz, difference, rotation_matrix, force_vector;
        position.setFromMatrixPosition(playerShipMesh.matrixWorld);
        ownx = position.x;
        ownz = position.z;
        if (enemys.length) {

            for (index = 0; index < enemys.length; ++index) {

                /* eslint-disable */
                position = new THREE.Vector3();
                /* eslint-enable */
                position.setFromMatrixPosition(enemys[index].matrixWorld);
                otherx = position.x;
                otherz = position.z;

                difference = Math.sqrt(((ownx - otherx) * (ownx - otherx)) + ((ownz - otherz) * (ownz - otherz)));

                if (difference < enemyWeaponRange * 50) {

                    if (enemys[index].userData.engineforward < 1000) {
                        /* eslint-disable */
                        rotation_matrix = new THREE.Matrix4().extractRotation(enemys[index].matrix);
                        force_vector = new THREE.Vector3(0, 0, (-1) * accelerationForwardEnemy).applyMatrix4(rotation_matrix);
                        /* eslint-enable */
                        enemys[index].applyCentralForce(force_vector);
                        enemys[index].userData.engineforward = enemys[index].userData.engineforward + 1;

                    } else {
                        if (enemys[index].userData.engineforward < 2000) {

                            enemys[index].userData.engineforward = enemys[index].userData.engineforward + 1;
                            if (enemys[index].userData.engineforward == 2000) {
                                enemys[index].userData.engineforward = 0;
                            }
                            if ((enemys[index].userData.engineforward > 1200) && (enemys[index].userData.engineforward < 1800)) {
                                /* eslint-disable */
                                rotation_matrix = new THREE.Matrix4().extractRotation(enemys[index].matrix);
                                /* eslint-enable */
                                enemys[index].applyForce(
                                    /* eslint-disable */
                                    new THREE.Vector3(0, 0, 20).applyMatrix4(rotation_matrix),
                                    new THREE.Vector3(turnSpeedEnemy * (-1), 0, 0).applyMatrix4(rotation_matrix));
                                /* eslint-enable */
                                enemys[index].applyForce(
                                    /* eslint-disable */
                                    new THREE.Vector3(0, 0, -20).applyMatrix4(rotation_matrix),
                                    new THREE.Vector3(turnSpeedEnemy, 0, 0).applyMatrix4(rotation_matrix));
                                /* eslint-enable */
                            }

                        }

                    }

                }
            }
        }

    }

    function enemyLogic() {
        /* eslint-disable */
        var position = new THREE.Vector3(),
            /* eslint-enable */
            ownx, ownz, index, otherx, otherz, difference, rotation_matrix, leftDmg, casterLeft, ownName, all, collisionsLeft, rightDmg, casterRight, collisionsRight;
        position.setFromMatrixPosition(playerShipMesh.matrixWorld);
        ownx = position.x;



        ownz = position.z;
        if (enemys.length) {

            for (index = 0; index < enemys.length; ++index) {

                /* eslint-disable */
                position = new THREE.Vector3();
                /* eslint-enable */
                position.setFromMatrixPosition(enemys[index].matrixWorld);
                otherx = position.x;
                otherz = position.z;

                difference = Math.sqrt(((ownx - otherx) * (ownx - otherx)) + ((ownz - otherz) * (ownz - otherz)));




                if (difference < enemyWeaponRange) {
                    /* eslint-disable */
                    rotation_matrix = new THREE.Matrix4().extractRotation(enemys[index].matrix);
                    leftDmg = enemys[index].position.clone().add((new THREE.Vector3(-45, 0, -20.0)).applyMatrix4(rotation_matrix));
                    casterLeft = new THREE.Raycaster();
                    /* eslint-enable */
                    casterLeft.set(leftDmg, playerShipMesh.position.clone().sub(leftDmg).normalize());

                    ownName = enemys[index].userData.name;
                    enemys[index].userData.name = "current";

                    all = allObjects.slice(0);
                    all.push(playerShipMesh);

                    collisionsLeft = casterLeft.intersectObjects(all);





                    if (collisionsLeft.length)

                    {

                        doNPCNPCDmg(laserDmgEnemy, collisionsLeft[0].object, false);
                        if (enemys[index].userData.shootReadyL == 0) {
                            showShootLeftEnemy(enemys[index], collisionsLeft[0].object, index);
                            enemys[index].userData.shootReadyL = 4;
                        } else {
                            enemys[index].userData.shootReadyL = enemys[index].userData.shootReadyL - 1;
                        }





                    }
                    /* eslint-disable */
                    rightDmg = enemys[index].position.clone().add((new THREE.Vector3(45, 0, -20.0)).applyMatrix4(rotation_matrix));
                    casterRight = new THREE.Raycaster();
                    /* eslint-enable */
                    casterRight.set(rightDmg, playerShipMesh.position.clone().sub(rightDmg).normalize());



                    collisionsRight = casterRight.intersectObjects(all);

                    if (collisionsRight.length)

                    {

                        doNPCNPCDmg(laserDmgEnemy, collisionsRight[0].object, false);
                        if (enemys[index].userData.shootReadyR == 0) {
                            showShootRightEnemy(enemys[index], collisionsRight[0].object, index);
                            enemys[index].userData.shootReadyR = 4;
                        } else {
                            enemys[index].userData.shootReadyR = enemys[index].userData.shootReadyR - 1;
                        }


                    }




                    enemys[index].userData.name = ownName;




                }

            }


        }
    }


    function showShootLeftEnemy(obj, obj2, ind) {
        var pointX, pointY, material, rotation_matrix, direction, orientation, edgeGeometry, edge;
        if (!laserBeamsLeftEnemy[ind].length) {
            /* eslint-disable */
            rotation_matrix = new THREE.Matrix4().extractRotation(obj.matrix);
            pointX = obj.position.clone().add((new THREE.Vector3(-45, 0, -20.0)).applyMatrix4(rotation_matrix));

            /* eslint-enable */
            pointY = obj2.position;

            material = laserMatirealEnemy;
            /* eslint-disable */
            direction = new THREE.Vector3().subVectors(pointY, pointX);
            orientation = new THREE.Matrix4();
            orientation.lookAt(pointX, pointY, new THREE.Object3D().up);
            orientation.multiply(new THREE.Matrix4().set(1, 0, 0, 0,
                /* eslint-enable */
                0, 0, 1, 0,
                0, -1, 0, 0,
                0, 0, 0, 1));

            /* eslint-disable */
            edgeGeometry = new THREE.CylinderGeometry(0.9, 0.9, direction.length(), 8, 1);
            edge = new THREE.Mesh(edgeGeometry, material);
            /* eslint-enable */
            edge.applyMatrix(orientation);

            edge.position.x = (pointY.x + pointX.x) / 2;
            edge.position.y = (pointY.y + pointX.y) / 2;
            edge.position.z = (pointY.z + pointX.z) / 2;

            laserBeamsLeftEnemy[ind].push(edge);
            scene.add(edge);


        }






    }

    function showShootRightEnemy(obj, obj2, ind) {
        var pointX, pointY, material, rotation_matrix, direction, orientation, edgeGeometry, edge;
        if (!laserBeamsRightEnemy[ind].length) {
            /* eslint-disable */
            rotation_matrix = new THREE.Matrix4().extractRotation(obj.matrix);
            pointX = obj.position.clone().add((new THREE.Vector3(45, 0, -20.0)).applyMatrix4(rotation_matrix));
            /* eslint-enable */

            pointY = obj2.position;

            material = laserMatirealEnemy;
            /* eslint-disable */
            direction = new THREE.Vector3().subVectors(pointY, pointX);
            orientation = new THREE.Matrix4();
            orientation.lookAt(pointX, pointY, new THREE.Object3D().up);
            orientation.multiply(new THREE.Matrix4().set(1, 0, 0, 0,
                /* eslint-enable */
                0, 0, 1, 0,
                0, -1, 0, 0,
                0, 0, 0, 1));
            /* eslint-disable */
            edgeGeometry = new THREE.CylinderGeometry(0.9, 0.9, direction.length(), 64, 1);
            edge = new THREE.Mesh(edgeGeometry, material);
            /* eslint-enable */
            edge.applyMatrix(orientation);

            edge.position.x = (pointY.x + pointX.x) / 2;
            edge.position.y = (pointY.y + pointX.y) / 2;
            edge.position.z = (pointY.z + pointX.z) / 2;




            laserBeamsRightEnemy[ind].push(edge);
            scene.add(edge);

        }






    }

    function updatePlayerStats() {
        playerHull = playerShipMesh.userData.live;
        playerShield = playerShipMesh.userData.shield;
    }

    function pushToMessageBox(message) {
        messagesAll.push(message);
        if (messagesAll.length > 5) {
            messagesAll.splice(0, 1);

        }

        dispatchNewEvent("newMessage", messagesAll);


    }

    function enemyShieldReg() {
        var currentShield;
        if (enemys.length) {
            for (let index = 0; index < enemys.length; index++) {
                currentShield = enemys[index].userData.shield;
                currentShield = currentShield + enemys[index].userData.shieldReg;
                if (currentShield > enemys[index].userData.MaxShield) {
                    enemys[index].userData.shield = enemys[index].userData.MaxShield;
                } else {
                    enemys[index].userData.shield = currentShield;

                }


            }

        }




    }

    function update10() {

        if (!pause) {

            getNearestObj();

        }
    }

    function update01() {

        if (!pause) {
            enemyShieldReg();


        }
    }



    function update30() {
        getFps(0);
        if (!pause) {
            updatePlayerStats();
            shootingUpdate();


            enemyLogic();
            keyworker.shoot();

        }
    }

    function update60() {
        getFps(2);
        if (!pause) {
            scene.simulate();


        }


    }

    function setFpsCounters(fpsCounters) {
        var lastCalledTimes = [fpsCounters];
        frameTimes = [fpsCounters];
        fps = [fpsCounters];
        for (let index = 0; index < fpsCounters; index++) {
            frameTimes[index] = [];
            lastCalledTimes[index] = Date.now();
            fps[index] = [];
        }



    }

    function getFps(number) {
        var frameTime = Date.now() - lastCalledTime[number],
            msPerallFrames = 0;

        lastCalledTime[number] = Date.now();

        if (frameTimes[number].length < 20) {
            frameTimes[number].push(frameTime);

        } else {
            for (let index = 0; index < frameTimes[number].length - 1; index++) {
                msPerallFrames = msPerallFrames + frameTimes[number][index];

            }
            fps[number] = 1000 / (msPerallFrames / frameTimes[number].length);
            frameTimes[number] = [];
        }
        return fps[number];


    }

    function autoQuality(fps) {
        var lFps = 0;
        longTermRenderFps.push(fps);


        if (longTermRenderFps.length > 50) {


            for (let index = 0; index < longTermRenderFps.length; index++) {
                lFps = lFps + longTermRenderFps[index];

            }

            lFps = lFps / longTermRenderFps.length;

            longTermRenderFps = [];

            if (lFps < fpsLimit - 5) {
                if (fpsLimit == 30) {
                    changeFps(25);

                }

                if (fpsLimit > 30) {
                    changeFps(30);

                }

                if (resolutionFactor > 0.3) {
                    resolutionFactor = resolutionFactor - 0.1;
                    setResolution(resolutionFactor);

                    qualityEvent(Math.round(resolutionFactor * 10) / 10);

                }
            }
            if (lFps > fpsLimit) {

                if ((fpsLimit == 45) && (fpsLimit < savedTargetFps)) {
                    changeFps(60);

                }

                if ((fpsLimit == 30) && (fpsLimit < savedTargetFps)) {

                    changeFps(45);

                }





                if (fpsLimit < 30) {
                    changeFps(30);

                } else {
                    if (resolutionFactor < 1) {
                        resolutionFactor = resolutionFactor + 0.1;
                        setResolution(resolutionFactor);

                        qualityEvent(Math.round(resolutionFactor * 10) / 10);

                    }
                }
            }




        }


    }

    function qualityEvent(resolutionFactor) {
        dispatchNewEvent("newResolutionFactor", resolutionFactor);




    }

    function fpsCurrentTargetEvent(target) {
        dispatchNewEvent("newFPSTarget", target);



    }

    function smoothFpsAnimate() {

        autoQuality(getFps(1));

        if (!pause) {

            camera.update();

            for (let index = 0; index < explosionNumber; index++) {
                explosionGroup[index].tick();
            }


            renderer.setViewport(0, 0, canvas.width, canvas.height);

            composer.render();



            renderer.clear(false, true, false);

            renderer.setViewport(0, 0, canvas.width / 5, canvas.height / 4);




            mapComposer.render();
        }



    }

    function unpauseGame() {

        pause = false;
        scene.onSimulationResume();
    }

    function pauseGame() {
        pause = true;
    }

    function getWeaponRg() {
        return weaponRange;
    }

    function getTheFps() {

        return fps;
    }



    function getCredits() {
        return credits;
    }

    function getPlayerShield() {

        return playerShield;
    }

    function getPlayerShieldMax() {

        return playerShipMesh.userData.MaxShield;
    }

    function getPlayerHull() {

        return playerHull;
    }

    function getPlayerHullMax() {

        return playerShipMesh.userData.MaxLive;
    }

    function getNearestEnemy() {
        var currentEnemyDistance = closesedEnemy,
            enemy,
            currentEnemyName, currentEnemyLife, currentEnemyShield, currentEnemyMaxShield;
        if (typeof closesdObjEnemy != "undefined") {

            currentEnemyName = closesdObjEnemy.userData.name;
            currentEnemyLife = closesdObjEnemy.userData.live;
            currentEnemyShield = closesdObjEnemy.userData.shield;
            currentEnemyMaxShield = closesdObjEnemy.userData.MaxShield;
        }

        enemy = {
            distance: currentEnemyDistance,
            name: currentEnemyName,
            life: currentEnemyLife,
            shield: currentEnemyShield,
            MaxShield: currentEnemyMaxShield
        };
        return enemy;
    }

    function getNearestObject() {
        var currentObjectDistance = closesed,
            currentObjectName, currentObjectLife,
            object;
        if (typeof closesdObj != "undefined") {

            currentObjectName = closesdObj.userData.name;
            currentObjectLife = closesdObj.userData.live;
        }

        object = {
            distance: currentObjectDistance,
            name: currentObjectName,
            life: currentObjectLife
        };
        return object;
    }

    function setPlayerHull(hu) {
        playerShipMesh.userData.live = hu;
        playerShipMesh.userData.MaxLive = hu;



    }

    function addShieldReg(sr) {
        if (!pause) {
            playerShipMesh.userData.shield = playerShipMesh.userData.shield + sr;
            if (playerShipMesh.userData.shield > playerShipMesh.userData.MaxShield) {
                playerShipMesh.userData.shield = playerShipMesh.userData.MaxShield;
            }
        }

    }

    function setPlayerShield(sh) {


        playerShipMesh.userData.MaxShield = sh;
        playerShipMesh.userData.shield = sh;
    }

    function setWeaponRg(wrg) {
        weaponRange = wrg;
    }

    function setMaxSpeed(msp) {
        maxSpeed = msp;
    }

    function setAcc(ac) {
        accelerationBack = accelerationBack + ac;
        accelerationSide = accelerationSide + ac;
        accelerationForward = accelerationForward + ac;
    }

    function setDamage(da) {
        laserDmgPlayer = da;
    }

    that.unpauseGame = unpauseGame;
    that.pauseGame = pauseGame;
    that.getPlayerShieldMax = getPlayerShieldMax;
    that.getPlayerHullMax = getPlayerHullMax;

    that.setPlayerHull = setPlayerHull;
    that.setPlayerShield = setPlayerShield;
    that.setWeaponRg = setWeaponRg;
    that.setMaxSpeed = setMaxSpeed;
    that.setAcc = setAcc;
    that.setDamage = setDamage;
    that.addShieldReg = addShieldReg;
    that.getFps = getTheFps;

    that.enableWireframeAsteroids = enableWireframeAsteroids;
    that.enableWireframeEnemys = enableWireframeEnemys;

    that.manuellShooting = manuellShooting;
    that.moveright = moveright;
    that.moveleft = moveleft;
    that.moveforward = moveforward;
    that.movebackward = movebackward;
    that.turnRight = turnRight;
    that.turnLeft = turnLeft;
    that.clearIntervalls = clearIntervalls;
    that.getPlayerHull = getPlayerHull;
    that.getPlayerShield = getPlayerShield;
    that.makeRestartPossible = makeRestartPossible;
    that.changeFps = changeFps;
    that.getCredits = getCredits;
    that.getNearestEnemy = getNearestEnemy;
    that.getNearestObject = getNearestObject;
    that.getSpeed = getSpeed;
    that.getWeaponRg = getWeaponRg;
    that.init = init;

    return that;
};
