"use strict";
var maincode = maincode || {};

maincode = (function() {

    var that = {},
        worldcode,
        flight_User_Interface,
        logic,
        keyworker,
        buttonWorker,
        userInterface,
        obj,
        enemy,

        wireMe = false,
        wireEn = false,

        shieldGain = 0,
        shieldRecovery = 10,
        ticksToShieldGain = 100,
        changeStatsTicks = 100,
        backgroundMusic,
        backgroundMusicVolume = 0.05,
        timer;




    function init() {

        worldcode = new maincode.worldcode();
        logic = new maincode.logic();
        flight_User_Interface = new maincode.flight_User_Interface();
        keyworker = new maincode.keyworker();
        buttonWorker = new maincode.buttonWorker();
        userInterface = new maincode.userInterface();





        initIt();

        changeStats();

        backgroundMusic = new Audio("src/Sound/bensound-scifi.mp3");
        backgroundMusic.loop = true;


        backgroundMusic.play();
        backgroundMusic.volume = backgroundMusicVolume;

        window.addEventListener("beforeunload", function() {
            clearTimeout(timer);
            worldcode.clearIntervalls();

        });
        window.addEventListener("HullUp", function() {
            worldcode.setPlayerHull(logic.getHull());
        });
        window.addEventListener("ShieldUp", function() {
            worldcode.setPlayerShield(logic.getShield());
        });
        window.addEventListener("WrgUp", function() {
            worldcode.setWeaponRg(logic.getWeaponRange());
        });
        window.addEventListener("SpeedUp", function() {
            worldcode.setMaxSpeed(logic.getMaxSpeed());
        });
        window.addEventListener("AccUp", function() {
            worldcode.setAcc(logic.getAcc());
        });
        window.addEventListener("DamageUp", function() {
            worldcode.setDamage(logic.getDamage());
        });
        window.addEventListener("RecoveryUp", function() {
            shieldRecovery = logic.getRecovery();
        });
        window.addEventListener("WireMeUp", function() {
            worldcode.enableWireframeAsteroids(true);
        });
        window.addEventListener("WireEnUp", function() {
            worldcode.enableWireframeEnemys(true);
        });


        window.addEventListener("saveLocaly", logic.saveLocaly);



        shieldRecovery = logic.getRecovery();
        if (logic.getLevels()[7] === 2) {
            wireMe = true;
        }

        if (logic.getLevels()[8] === 2) {
            wireEn = true;
        }
    }

    function initIt() {
        logic.initData();
        worldcode.init(keyworker, logic, buttonWorker);

        flight_User_Interface.initFlightUI(logic);
        keyworker.initKeys(worldcode, logic);
        buttonWorker.initButtonWorker(worldcode, logic);
        userInterface.initUi(logic);
        logic.getTargetFps();


    }



    function changeStats() {
        enemy = worldcode.getNearestEnemy();
        obj = worldcode.getNearestObject();

        flight_User_Interface.changeEnemyDis(Math.round(enemy.distance));
        flight_User_Interface.changeEnemyName(enemy.name);
        flight_User_Interface.changeEnemyLife(Math.round(enemy.life));
        flight_User_Interface.changeEnemyShield(Math.round(enemy.shield), Math.round(enemy.MaxShield));

        flight_User_Interface.changeObjDis(Math.round(obj.distance));
        flight_User_Interface.changeObjName(obj.name);

        flight_User_Interface.changeObjLife(Math.round(obj.life));

        flight_User_Interface.refreshFps(Math.round(worldcode.getFps()[1]), Math.round(worldcode.getFps()[0]), Math.round(worldcode.getFps()[2]));

        flight_User_Interface.changeCredits(logic.getCredits());
        flight_User_Interface.changeHull(Math.round(worldcode.getPlayerHull()), Math.round(worldcode.getPlayerHullMax()));
        flight_User_Interface.changeSchield(Math.round(worldcode.getPlayerShield()), Math.round(worldcode.getPlayerShieldMax()));
        flight_User_Interface.changeSpeed(Math.round(worldcode.getSpeed() * 10));
        flight_User_Interface.changeWeaponRg(worldcode.getWeaponRg());

        if (wireMe) {
            if (worldcode.enableWireframeAsteroids(true)) {
                wireMe = false;
            }
        }

        if (wireEn) {
            if (worldcode.enableWireframeEnemys(true)) {
                wireEn = false;
            }
        }


        shieldGain = shieldGain + 1;

        if (shieldGain === ticksToShieldGain) {
            worldcode.addShieldReg(shieldRecovery);

            shieldGain = 0;
        }

        timer = setTimeout(changeStats, changeStatsTicks);
    }

    that.init = init;

    return that;
}());
