"use strict";
var maincode = maincode || {};
maincode.userInterface = function() {
    var that = {},
        ui1 = document.getElementById("di"),
        ui2 = document.getElementById("di2"),
        startScreen = document.getElementById("di3"),

        descriptionText = document.getElementById("di5"),
        shopScreen = document.getElementById("di6"),
        optionsScreen = document.getElementById("di7"),
        deadScreen = document.getElementById("di8"),


        buttonStart = document.getElementById("b2"),




        shopCost = document.getElementById("shopCost"),

        buttonShop3 = document.getElementById("buttonShop3"),


        buttonOptions3 = document.getElementById("buttonOptions3"),


        shopDescription = document.getElementById("shopDescription"),
        upgradeTimes = document.getElementById("upgradeTimes"),
        shopImage = document.getElementById("shopimage"),
        inputField = document.getElementById("saveInput"),

        startStory = document.getElementById("startStory"),
        logic;









    function initUi(logik) {
        logic = logik;


        buttonShop3.style.left = (window.innerWidth / 2) - (buttonShop3.offsetWidth / 2);
        buttonOptions3.style.left = (window.innerWidth / 2) - (buttonOptions3.offsetWidth / 2);


        window.addEventListener("resize", onWindowResize, false);

        window.addEventListener("showPauseScreen", showPauseScreen);
        window.addEventListener("hidePauseScreen", hidePauseScreen);
        window.addEventListener("showDeadScreen", showDeadScreen);
        window.addEventListener("restartIt", restartIt);
        window.addEventListener("hideShopScreen", hideShopScreen);
        window.addEventListener("showOptions", showOptions);
        window.addEventListener("hideOptions", hideOptions);
        window.addEventListener("setShopCostText", setShopCostText);
        window.addEventListener("setShopDescriptionText", setShopDescriptionText);
        window.addEventListener("setShopUpgradeTimes", setShopUpgradeTimes);
        window.addEventListener("setShopImage", setShopImage);
        window.addEventListener("setStartButtonContent", setStartButtonContent);
        window.addEventListener("loadLocaly", loadLocaly);


        inputField.addEventListener("change", handleFileSelect, false);




        if (!logic.getStartStory() == 0) {


            startStory.innerHTML = "<p><e>After your uncle died, you started exploring the universe. You are still stranded in a hostile system full with pirates and smugglers, but you upgrade your ship more and more to make a modern figth ship out of your old vessel.Good Luck.</e></p>";
        }
        logic.setStartStory(1);

    }

    function loadLocaly() {

        inputField.click();

    }

    function onWindowResize() {

        buttonOptions3.style.left = (window.innerWidth / 2) - (buttonOptions3.offsetWidth / 2);
        buttonShop3.style.left = (window.innerWidth / 2) - (buttonShop3.offsetWidth / 2);

    }

    function handleFileSelect(evt) {

        var files = evt.target.files,
            i, file, reader;




        for (i = 0; i < files.length; i++) {
            file = files[i];
            if (file.name.split(".").pop() !== "save") {
                /*eslint-disable */
                alert(file.name + " is not a .save file");
                /*eslint-enable */
                return false;
            }
            reader = new FileReader();


            reader.onload = (fileLoaded)(file);



            reader.readAsText(file);



        }
        return true;
    }

    function fileLoaded() {

        return function(e) {
            logic.saveLoaded(e.target.result);

        };


    }





    function restartIt() {

        ui1.className = "topleftcorner";
        ui2.className = "toprightcorner";
        deadScreen.className = "startScreenInvisible";

        startScreen.className = "startScreen";
        descriptionText.className = "startScreen";

    }

    function setStartButtonContent(event) {
        buttonStart.innerHTML = "<p style=font-size:3vw><e>" + event.detail + "</e></p>";


    }

    function setShopImage(event) {
        shopImage.src = event.detail;


    }

    function setShopUpgradeTimes(event) {

        upgradeTimes.innerHTML = "<e>" + event.detail + "</e>";


    }

    function setShopDescriptionText(event) {
        shopDescription.innerHTML = "<e>" + event.detail + "</e>";


    }

    function setShopCostText(event) {

        shopCost.innerHTML = event.detail;

    }

    function showDeadScreen() {
        descriptionText.className = "startScreenInvisible";
        ui1.className = "startScreenInvisible";
        ui2.className = "startScreenInvisible";
        deadScreen.className = "startScreen";


    }

    function showOptions() {
        descriptionText.className = "startScreenInvisible";

        optionsScreen.className = "startScreen";

    }

    function hideOptions() {
        descriptionText.className = "startScreen";
        optionsScreen.className = "startScreenInvisible";


    }

    function showPauseScreen() {

        document.getElementById("di5").className = "startScreen";
        startScreen.className = "startScreen";


    }

    function hideShopScreen() {
        descriptionText.className = "startScreen";
        shopScreen.className = "startScreenInvisible";

    }

    function hidePauseScreen() {
        startScreen.className = "startScreenInvisible";
        descriptionText.className = "startScreenInvisible";
        shopScreen.className = "startScreenInvisible";

    }


    that.initUi = initUi;
    return that;
};
