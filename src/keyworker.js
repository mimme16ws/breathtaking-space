"use strict";
var maincode = maincode || {};
maincode.keyworker = function() {


    var that = {},
        worldcode,


        gameLoaded = 0,
        paused = true,
        pausePressedBefore = false,
        keyMap = [];



    function initKeys(world) {
        worldcode = world;

        addListeners();

    }

    function addListeners() {
        document.addEventListener("keydown", move);
        document.addEventListener("keyup", move);
        window.addEventListener("gameLoadingChanged", gameLoadChanged);


    }





    function gameLoadChanged(event) {

        gameLoaded = event.detail;
        paused = event.detail2;
        pausePressedBefore = event.detail3;

    }






    function dispatchNewEvent(eventName, detail) {
        var event = new CustomEvent(eventName, {
            "detail": detail
        });
        window.dispatchEvent(event);


    }


    function pause() {
        if (!pausePressedBefore) {
            if (!paused) {
                worldcode.pauseGame();

                dispatchNewEvent("showPauseScreen", "");
                dispatchNewEvent("paused", true);

                paused = true;
            } else {


                dispatchNewEvent("hidePauseScreen", "");
                worldcode.unpauseGame();
                dispatchNewEvent("paused", false);
                paused = false;
            }

            pausePressedBefore = true;

        }

    }


    function move(e) {


        keyMap[e.keyCode] = e.type == "keydown";
        if (keyMap[13] == true) {
            e.stopPropagation();
            e.preventDefault();
        }

        if (keyMap[32] == true) {
            e.stopPropagation();
            e.preventDefault();
        }
        if (keyMap[80] == true) {

            if (gameLoaded >= 3) {

                pause();
            }

        }
        if (keyMap[80] == false) {
            if (gameLoaded >= 3) {
                if (pausePressedBefore) {

                    pausePressedBefore = false;
                }
            }
        }
    }

    function moveIt() {
        if (keyMap[65] == true) {
            worldcode.moveleft();
        }

        if (keyMap[68] == true) {
            worldcode.moveright();
        }

        if (keyMap[87] == true) {
            worldcode.moveforward();
        }

        if (keyMap[83] == true) {
            worldcode.movebackward();
        }

        if (keyMap[81] == true) {
            worldcode.turnLeft();
        }

        if (keyMap[69] == true) {
            worldcode.turnRight();
        }


    }

    function shoot() {
        if (keyMap[32] == true) {
            worldcode.manuellShooting();
        }
    }

    that.moveIt = moveIt;
    that.shoot = shoot;
    that.initKeys = initKeys;
    return that;

};
