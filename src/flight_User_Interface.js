"use strict";
var maincode = maincode || {};
maincode.flight_User_Interface = function() {
    var that = {},
        credits,


        objDis,
        objName,
        objLife,


        hull,
        shield,
        speed,
        weaponRg,

        rightBar,
        leftBar,

        objectDistanceValue,
        objectNameValue,
        objectLifeValue,

        enemyDisValue,
        enemyNameValue,
        enemyLifeValue,
        enemyShieldValue,
        enemyMaxShieldValue,


        enemyDis,
        enemyName,
        enemyLife,
        enemyShield,


        logic,




        fpsTarget = 30,
        resolutionFactor = 1,
        playerCredits = 0,
        playerHullpoints = 100,
        playerShieldpoints = 100,
        playerSpeed = 0,
        playerWeaponRange = 0,
        FPSDisplay,
        messageList,

        shopCredits;


    function initFlightUI(status) {


        enemyDisValue = "not available";
        enemyNameValue = "not available";
        enemyLifeValue = "not available";
        enemyShieldValue = "not available";
        enemyMaxShieldValue = "not available";

        objectDistanceValue = "not available";
        objectNameValue = "not available";
        objectLifeValue = "not available";


        logic = status;

        FPSDisplay = document.getElementById("FPSDisplay");
        leftBar = document.getElementById("di");
        leftBar.style.position = "fixed";
        leftBar.style.top = 0;
        leftBar.style.left = 0;

        rightBar = document.getElementById("di2");
        rightBar.style.position = "fixed";
        rightBar.style.top = 0;
        rightBar.style.right = 0;

        messageList = document.getElementById("messageList");




        enemyDis = document.getElementById("nePoints");
        enemyName = document.getElementById("ePoints");
        enemyLife = document.getElementById("elPoints");
        enemyShield = document.getElementById("esPoints");

        objDis = document.getElementById("noPoints");
        objName = document.getElementById("oPoints");
        objLife = document.getElementById("olPoints");


        credits = document.getElementById("cPoints");
        hull = document.getElementById("hPoints");
        shield = document.getElementById("sPoints");
        speed = document.getElementById("spPoints");
        weaponRg = document.getElementById("wrPoints");



        shopCredits = document.getElementById("shopCredits");

        enemyDis.innerHTML = "<p><c>" + enemyDisValue + "</c></p>";
        enemyName.innerHTML = "<p><c>" + enemyNameValue + "</c></p>";
        enemyLife.innerHTML = "<p><c>" + enemyLifeValue + "</c></p>";
        enemyShield.innerHTML = "<p><c>" + enemyShieldValue + "</c></p>";

        objDis.innerHTML = "<p><d>" + objectDistanceValue + "</d></p>";
        objName.innerHTML = "<p><d>" + objectNameValue + "</d></p>";
        objLife.innerHTML = "<p><d>" + objectLifeValue + "</d></p>";


        credits.innerHTML = "<p><b>" + playerCredits + "</b></p>";
        hull.innerHTML = "<p><b>" + playerHullpoints + "</b></p>";
        shield.innerHTML = "<p><b>" + playerShieldpoints + "</b></p>";
        speed.innerHTML = "<p><b>" + playerSpeed + "</b></p>";
        weaponRg.innerHTML = "<p><b>" + playerWeaponRange + "</b></p>";




        window.addEventListener("upgradeShop", onShopUpgraded);
        window.addEventListener("newMessage", onMessageReceived);
        window.addEventListener("newResolutionFactor", newResolutionFactor);
        window.addEventListener("newFPSTarget", newFPSTarget);



    }








    function newFPSTarget(event) {
        fpsTarget = event.detail;
    }

    function newResolutionFactor(event) {
        resolutionFactor = event.detail;
    }

    function onMessageReceived(event) {



        updateMessageBox(event.detail);
    }

    function updateMessageBox(messages) {


        var list = document.createElement("ul"),
            i, item;


        for (i = messages.length - 1; i >= 0; i--) {

            item = document.createElement("li");


            item.appendChild(document.createTextNode(messages[i]));


            list.appendChild(item);
        }

        messageList.innerHTML = "<p></p>";
        messageList.appendChild(list);



    }



    function onShopUpgraded(event) {
        var upgradeNum = event.detail;

        if (upgradeNum === 0) {
            onHullClicked();
        }
        if (upgradeNum === 1) {
            onShieldClicked();
        }
        if (upgradeNum === 2) {
            onRecoveryClicked();
        }

        if (upgradeNum === 3) {
            onDamageClicked();
        }
        if (upgradeNum === 4) {
            onWeaponClicked();
        }

        if (upgradeNum === 5) {
            onSpeedClicked();
        }
        if (upgradeNum === 6) {
            onAccClicked();
        }


        if (upgradeNum === 7) {
            onWireMeClicked();
        }
        if (upgradeNum === 8) {
            onWireEnClicked();
        }
    }







    function onHullClicked() {
        var cred = logic.getCredits();
        var costs = logic.getCostsHull()[logic.getLevels()[0] - 1];
        if (cred >= costs) {
            logic.safeCredits(-(logic.getCostsHull()[logic.getLevels()[0] - 1]));
            logic.safeHull(100);
            logic.setLevel(0, logic.getLevels()[0] + 1);
            dispatchNewEvent("HullUp");


        }
    }

    function onShieldClicked() {
        var cred = logic.getCredits();
        var costs = logic.getCostsShield()[logic.getLevels()[1] - 1];
        if (cred >= costs) {
            logic.safeCredits(-(logic.getCostsShield()[logic.getLevels()[1] - 1]));
            logic.safeShield(100);
            logic.setLevel(1, logic.getLevels()[1] + 1);
            dispatchNewEvent("ShieldUp");

        }
    }

    function onRecoveryClicked() {
        var cred = logic.getCredits();
        var costs = logic.getCostsRecovery()[logic.getLevels()[2] - 1];
        if (cred >= costs) {
            logic.safeCredits(-(logic.getCostsRecovery()[logic.getLevels()[2] - 1]));
            logic.safeRecovery(2);
            logic.setLevel(2, logic.getLevels()[2] + 1);
        }
        dispatchNewEvent("RecoveryUp");

    }

    function onDamageClicked() {
        var cred = logic.getCredits();
        var costs = logic.getCostsDamage()[logic.getLevels()[3] - 1];
        if (cred >= costs) {
            logic.safeCredits(-(logic.getCostsDamage()[logic.getLevels()[3] - 1]));
            logic.safeDamage(0.05);
            logic.setLevel(3, logic.getLevels()[3] + 1);
            dispatchNewEvent("DamageUp");

        }
    }

    function onWeaponClicked() {
        var cred = logic.getCredits();
        var costs = logic.getCostsWeapon()[logic.getLevels()[4] - 1];
        if (cred >= costs) {
            logic.safeCredits(-(logic.getCostsWeapon()[logic.getLevels()[4] - 1]));
            logic.safeWeaponRange(100);
            logic.setLevel(4, logic.getLevels()[4] + 1);
            dispatchNewEvent("WrgUp");
        }
    }

    function onSpeedClicked() {
        var cred = logic.getCredits();
        var costs = logic.getCostsSpeed()[logic.getLevels()[5] - 1];
        if (cred >= costs) {
            logic.safeCredits(-(logic.getCostsSpeed()[logic.getLevels()[5] - 1]));
            logic.safeMaxSpeed(100);
            logic.setLevel(5, logic.getLevels()[5] + 1);
            dispatchNewEvent("SpeedUp");

        }
    }

    function onAccClicked() {
        var cred = logic.getCredits();
        var costs = logic.getCostsAcc()[logic.getLevels()[6] - 1];
        if (cred >= costs) {
            logic.safeCredits(-(logic.getCostsAcc()[logic.getLevels()[6] - 1]));
            logic.safeAcc(logic.getAcc() + 3);
            logic.setLevel(6, logic.getLevels()[6] + 1);
            dispatchNewEvent("AccUp");
        }
    }





    function onWireMeClicked() {
        var cred = logic.getCredits();
        var costs = logic.getCostsWireMe()[logic.getLevels()[7] - 1];
        if (cred >= costs) {
            logic.safeCredits(-(logic.getCostsWireMe()[logic.getLevels()[7] - 1]));
            logic.setLevel(7, logic.getLevels()[7] + 1);
        }
        dispatchNewEvent("WireMeUp");
    }

    function onWireEnClicked() {
        var cred = logic.getCredits();
        var costs = logic.getCostsWireEn()[logic.getLevels()[8] - 1];
        if (cred >= costs) {
            logic.safeCredits(-(logic.getCostsWireEn()[logic.getLevels()[8] - 1]));
            logic.setLevel(8, logic.getLevels()[8] + 1);
        }
        dispatchNewEvent("WireEnUp");

    }





    function changeEnemyDis(dis) {
        if (dis < Number.MAX_VALUE) {
            enemyDisValue = dis;
        }
        enemyDis.innerHTML = "<p><c>" + enemyDisValue + "</c></p>";
    }

    function changeEnemyName(name) {
        if (typeof name != "undefined") {
            enemyNameValue = name;
        }
        enemyName.innerHTML = "<p><c>" + enemyNameValue + "</c></p>";
    }

    function changeEnemyLife(life) {
        if (!isNaN(life)) {
            enemyLifeValue = life;
        }
        enemyLife.innerHTML = "<p><c>" + enemyLifeValue + "</c></p>";
    }

    function changeEnemyShield(shield, maxShield) {
        var shieldString;
        if (!isNaN(shield)) {
            enemyShieldValue = shield;
        }
        if (!isNaN(shield)) {
            enemyMaxShieldValue = maxShield;
        }
        shieldString = enemyShieldValue + "/" + enemyMaxShieldValue;
        enemyShield.innerHTML = "<p><c>" + shieldString + "</c></p>";
    }

    function changeObjDis(distance) {
        if (distance < Number.MAX_VALUE) {
            objectDistanceValue = distance;
        }
        objDis.innerHTML = "<p><d>" + objectDistanceValue + "</d></p>";
    }

    function changeObjName(name) {

        if (typeof name != "undefined") {
            objectNameValue = name;
        }
        objName.innerHTML = "<p><d>" + objectNameValue + "</d></p>";
    }

    function changeObjLife(life) {
        if (!isNaN(life)) {
            objectLifeValue = life;
        }
        objLife.innerHTML = "<p><d>" + objectLifeValue + "</d></p>";
    }

    function changeCredits(creditPoints) {
        var credittext;
        playerCredits = creditPoints;
        credits.innerHTML = "<p><b>" + makeDigitLong(playerCredits) + "</b></p>";

        credittext = "Your Credits: " + makeDigitLong(playerCredits);

        shopCredits.innerHTML = credittext;
    }

    function changeHull(hullPoints, hullPointsMax) {
        var hullString;
        playerHullpoints = hullPoints;
        hullString = playerHullpoints + "/" + hullPointsMax;
        hull.innerHTML = "<p><b>" + hullString + "</b></p>";
    }

    function changeShield(shieldPoints, shieldPointsMax) {
        var shieldString,
            playerShieldpoints = shieldPoints;

        shieldString = playerShieldpoints + "/" + shieldPointsMax;
        shield.innerHTML = "<p><b>" + shieldString + "</b></p>";

    }

    function changeSpeed(speedPoints) {
        var maxSpeed;
        playerSpeed = makeDigitLong(speedPoints);
        maxSpeed = logic.getMaxSpeed() * 10;
        speed.innerHTML = "<p><b>" + playerSpeed + "/" + maxSpeed + "</b></p>";
    }

    function changeWeaponRg(weapRg) {
        playerWeaponRange = weapRg;
        weaponRg.innerHTML = "<p><b>" + playerWeaponRange + "</b></p>";
    }

    function makeDigitLong(digit) {
        var longDigit;
        if (digit > 999) {
            longDigit = digit;
        } else {
            if (digit > 99) {
                longDigit = "0" + digit;
            } else {
                if (digit > 9) {
                    longDigit = "00" + digit;
                } else {

                    longDigit = "000" + digit;

                }
            }
        }
        return longDigit;
    }

    function dispatchNewEvent(eventName, detail, detail2, detail3) {
        var event = new CustomEvent(eventName, {
            "detail": detail,
            "detail2": detail2,
            "detail3": detail3
        });
        window.dispatchEvent(event);


    }

    function refreshFps(renderFps, logicFps, physicFps) {

        var FpsTextLine = "Render FPS: " + renderFps + " Logic FPS: " + logicFps + " Physic FPS: " + physicFps;
        var qualityTextLine = "TargetFps: " + fpsTarget + " ResolutionFactor: " + resolutionFactor;
        FPSDisplay.innerHTML = "<p><f>" + FpsTextLine + "</f></p>" + "<p><f>" + qualityTextLine + "</f></p>";
    }

    that.updateMessageBox = updateMessageBox;
    that.refreshFps = refreshFps;
    that.changeEnemyDis = changeEnemyDis;
    that.changeEnemyName = changeEnemyName;
    that.changeEnemyLife = changeEnemyLife;
    that.changeEnemyShield = changeEnemyShield;
    that.changeObjDis = changeObjDis;
    that.changeObjName = changeObjName;
    that.changeObjLife = changeObjLife;
    that.changeCredits = changeCredits;
    that.changeHull = changeHull;
    that.changeSchield = changeShield;
    that.changeSpeed = changeSpeed;
    that.changeWeaponRg = changeWeaponRg;
    that.initFlightUI = initFlightUI;
    that.initFlightUI = initFlightUI;
    return that;
};
