"use strict";
var maincode = maincode || {};
maincode.logic = function() {

    var that = {},
        credits = 0,
        maxSpeed = 100,
        weaponRange = 1750,
        hull = 100,
        shield = 100,
        acc = 0,
        damage = 0.1,
        recovery = 10,
        standardTargetFps = 30,
        targetFps = 0,
        startStroy = 0,
        costsHull = [100, 200, 300, 500, 750, 1000, 2000, 3000, 4000, 5000],
        costsShield = [100, 200, 300, 500, 750, 1000, 2000, 3000, 4000, 5000],
        costsWeapon = [100, 200, 500, 750, 1000, 5000],
        costsSpeed = [100, 500, 1000, 5000],
        costsAcceleration = [100, 300, 500, 750, 1000],
        costsDamage = [100, 200, 300, 400, 500, 1000, 2000],
        costsRecovery = [50, 100, 150, 200, 250],
        costsWireMe = [100],
        costsWireEn = [150],
        levels = [1, 1, 1, 1, 1, 1, 1, 1, 1];

    function initData() {

        if (localStorage.getItem("Credits") !== null) {
            credits = parseInt(localStorage.getItem("Credits"));
        }

        if (localStorage.getItem("MaxSpeed") !== null) {
            maxSpeed = parseInt(localStorage.getItem("MaxSpeed"));
        }

        if (localStorage.getItem("WeaponRange") !== null) {
            weaponRange = parseInt(localStorage.getItem("WeaponRange"));
        }

        if (localStorage.getItem("Hull") !== null) {
            hull = parseInt(localStorage.getItem("Hull"));
        }

        if (localStorage.getItem("Shield") !== null) {
            shield = parseInt(localStorage.getItem("Shield"));
        }

        if (localStorage.getItem("Acc") !== null) {
            acc = parseInt(localStorage.getItem("Acc"));
        }

        if (localStorage.getItem("Damage") !== null) {
            damage = parseFloat(localStorage.getItem("Damage"));
        }

        if (localStorage.getItem("Recovery") !== null) {
            recovery = parseInt(localStorage.getItem("Recovery"));
        }
        if (localStorage.getItem("ShowStartStory") !== null) {
            startStroy = parseInt(localStorage.getItem("ShowStartStory"));
        }
        if (localStorage.getItem("FPSTarget") !== null) {

            targetFps = parseInt(localStorage.getItem("FPSTarget"));
            getTargetFps();
        } else {
            targetFps = standardTargetFps;
            getTargetFps();
        }

        readLevels();

    }

    function saveLocaly() {
        var dataToSave = credits + " " + maxSpeed + " " + levels[5] + " " + acc + " " + levels[6] + " " + damage + " " + levels[3] + " " + weaponRange + " " + levels[4] + " " + hull + " " + levels[0] + " " + shield + " " + levels[1] + " " + recovery + " " + levels[2] + " " + levels[7] + " " + levels[8] + " " + targetFps + " " + startStroy,
            blob = new Blob([dataToSave], {
                type: "text/plain;charset=utf-8"
            }),
            date = new Date();
        /*eslint-disable */
        saveAs(blob, "SpaceGameSave_" + date.getHours() + "." + date.getMinutes() + " Uhr " + date.getDate() + "." + date.getMonth() + "." + date.getFullYear() + ".save");
        /*eslint-enable */

    }

    function safeLevels() {
        localStorage.setItem("LevelHull", levels[0]);

        localStorage.setItem("LevelShield", levels[1]);
        localStorage.setItem("LevelRecovery", levels[2]);

        localStorage.setItem("LevelDamage", levels[3]);
        localStorage.setItem("LevelWeapon", levels[4]);

        localStorage.setItem("LevelSpeed", levels[5]);
        localStorage.setItem("LevelAcc", levels[6]);


        localStorage.setItem("LevelWireMe", levels[7]);
        localStorage.setItem("LevelWireEn", levels[8]);

    }

    function saveLoaded(loadedString) {


        var loadString = loadedString.split(" ");

        safeCredits(loadString[0] - getCredits());



        safeMaxSpeed(loadString[1] - getMaxSpeed());
        setLevel(5, loadString[2]);

        safeAcc(loadString[3]);
        setLevel(6, loadString[4]);

        safeDamage(loadString[5] - getDamage());
        setLevel(3, loadString[6]);

        safeWeaponRange(loadString[7] - getWeaponRange());
        setLevel(4, loadString[8]);



        safeHull(loadString[9] - getHull());
        setLevel(0, loadString[10]);




        safeShield(loadString[11] - getShield());
        setLevel(1, loadString[12]);

        safeRecovery(loadString[13] - getRecovery());
        setLevel(2, loadString[14]);


        setLevel(7, loadString[15]);
        setLevel(8, loadString[16]);

        safeFpsOption(loadString[17]);
        setStartStory(loadString[18] - getStartStory());
        location.reload(false);

    }

    function readLevels() {

        if (localStorage.getItem("LevelHull") !== null) {
            levels[0] = parseInt(localStorage.getItem("LevelHull"));
        }


        if (localStorage.getItem("LevelShield") !== null) {
            levels[1] = parseInt(localStorage.getItem("LevelShield"));
        }
        if (localStorage.getItem("LevelRecovery") !== null) {
            levels[2] = parseInt(localStorage.getItem("LevelRecovery"));
        }


        if (localStorage.getItem("LevelDamage") !== null) {
            levels[3] = parseInt(localStorage.getItem("LevelDamage"));
        }
        if (localStorage.getItem("LevelWeapon") !== null) {
            levels[4] = parseInt(localStorage.getItem("LevelWeapon"));
        }


        if (localStorage.getItem("LevelSpeed") !== null) {
            levels[5] = parseInt(localStorage.getItem("LevelSpeed"));
        }
        if (localStorage.getItem("LevelAcc") !== null) {
            levels[6] = parseInt(localStorage.getItem("LevelAcc"));
        }



        if (localStorage.getItem("LevelWireMe") !== null) {
            levels[7] = parseInt(localStorage.getItem("LevelWireMe"));
        }
        if (localStorage.getItem("LevelWireEn") !== null) {
            levels[8] = parseInt(localStorage.getItem("LevelWireEn"));
        }

    }

    function getCredits() {
        return credits;
    }

    function getMaxSpeed() {
        return maxSpeed;
    }

    function getWeaponRange() {
        return weaponRange;
    }

    function getHull() {
        return hull;
    }

    function getShield() {
        return shield;
    }

    function getAcc() {
        return acc;
    }

    function getDamage() {
        return damage;
    }

    function getRecovery() {
        return recovery;
    }

    function safeCredits(cred) {
        credits = credits + cred;
        localStorage.setItem("Credits", credits);
    }

    function setStartStory(startStory) {
        startStroy = startStroy + startStory;
        localStorage.setItem("ShowStartStory", startStroy);
    }

    function safeMaxSpeed(msp) {
        maxSpeed = maxSpeed + msp;
        localStorage.setItem("MaxSpeed", maxSpeed);
    }

    function safeWeaponRange(wrg) {
        weaponRange = weaponRange + wrg;
        localStorage.setItem("WeaponRange", weaponRange);
    }

    function safeFpsOption(fps) {
        targetFps = fps;
        dispatchNewEvent("changeFpsTarget", targetFps);
        localStorage.setItem("FPSTarget", targetFps);
    }

    function safeHull(hu) {
        hull = hull + hu;
        localStorage.setItem("Hull", hull);
    }

    function safeShield(sh) {
        shield = shield + sh;
        localStorage.setItem("Shield", shield);
    }

    function safeAcc(ac) {
        acc = ac;
        localStorage.setItem("Acc", acc);
    }

    function safeDamage(da) {
        damage = damage + da;
        localStorage.setItem("Damage", damage);
    }

    function safeRecovery(re) {
        recovery = recovery + re;
        localStorage.setItem("Recovery", recovery);
    }

    function getCostsHull() {
        return costsHull;
    }

    function getCostsShield() {
        return costsShield;
    }

    function getCostsWeapon() {
        return costsWeapon;
    }

    function getCostsSpeed() {
        return costsSpeed;
    }

    function getStartStory() {
        return startStroy;
    }

    function getCostsAcc() {
        return costsAcceleration;
    }

    function getCostsDamage() {
        return costsDamage;
    }

    function getCostsRecovery() {
        return costsRecovery;
    }

    function getCostsWireMe() {
        return costsWireMe;
    }

    function getCostsWireEn() {
        return costsWireEn;
    }

    function getLevels() {
        return levels;
    }

    function setLevel(index, level) {
        levels[index] = level;
        safeLevels();
    }

    function getTargetFps() {

        dispatchNewEvent("changeFpsTarget", targetFps);

        return targetFps;
    }

    function dispatchNewEvent(eventName, detail, detail2) {
        var event = new CustomEvent(eventName, {
            "detail": detail,
            "detail2": detail2
        });
        window.dispatchEvent(event);


    }


    that.getStartStory = getStartStory;
    that.setStartStory = setStartStory;
    that.saveLoaded = saveLoaded;
    that.setLevel = setLevel;
    that.getCostsHull = getCostsHull;
    that.getCostsShield = getCostsShield;
    that.getCostsWeapon = getCostsWeapon;
    that.getCostsSpeed = getCostsSpeed;
    that.getCostsAcc = getCostsAcc;
    that.getCostsDamage = getCostsDamage;
    that.getCostsRecovery = getCostsRecovery;
    that.getCostsWireMe = getCostsWireMe;
    that.getCostsWireEn = getCostsWireEn;
    that.getLevels = getLevels;

    that.getCredits = getCredits;
    that.getMaxSpeed = getMaxSpeed;
    that.getWeaponRange = getWeaponRange;
    that.getHull = getHull;
    that.getShield = getShield;
    that.getAcc = getAcc;
    that.getDamage = getDamage;
    that.getRecovery = getRecovery;

    that.safeFpsOption = safeFpsOption;
    that.safeLevels = safeLevels;
    that.safeCredits = safeCredits;
    that.safeMaxSpeed = safeMaxSpeed;
    that.safeWeaponRange = safeWeaponRange;
    that.safeHull = safeHull;
    that.safeShield = safeShield;
    that.safeAcc = safeAcc;
    that.safeDamage = safeDamage;
    that.safeRecovery = safeRecovery;
    that.saveLocaly = saveLocaly;
    that.getTargetFps = getTargetFps;

    that.initData = initData;

    return that;
};
