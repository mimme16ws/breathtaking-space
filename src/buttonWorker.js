"use strict";
var maincode = maincode || {};
maincode.buttonWorker = function() {


    var that = {},
        worldcode,
        restart = false,
        upgradeNumber = 0,
        gameLoaded = 0,
        paused = true,
        pausePressedBefore = false,

        shopImages = ["src/Images/hull.jpg",

            "src/Images/shield.jpg",
            "src/Images/shield_reg.jpg",

            "src/Images/weapon.jpg",
            "src/Images/weapon_range.jpg",

            "src/Images/engine.jpg",
            "src/Images/engine_acc.jpg",


            "src/Images/radar.jpg",
            "src/Images/radarEnemy.jpg"
        ],


        shopDescriptions = ["Buy this upgrade to improve the damage your hull can withstand before your ship get destroyed.",

            "Buy this upgrade to improve the damage your shield can withstand before your it fails.",
            "Buy this upgrade to improve the recovery of your shield",

            "Buy this upgrade to improve the damage of your weapons.",
            "Buy this upgrade to improve the range of your weapons.",

            "Buy this upgrade to improve the maximal speed of your ship.",
            "Buy this upgrade to improve the acceleration of your ship.",


            "Buy this upgrade to enable a visual marker around asteroids which tells you if they're damaged",
            "Buy this upgrade to enable a visual marker around your enemys which tells you if they're damaged"
        ],



        startScreenText = document.getElementById("di4"),
        descriptionText = document.getElementById("di5"),
        shopScreen = document.getElementById("di6"),

        saveButton = document.getElementById("saveButton"),
        loadButton = document.getElementById("loadButton"),

        buttonOkay = document.getElementById("b1"),
        buttonStart = document.getElementById("b2"),
        buttonShop = document.getElementById("b3"),
        buttonOptions = document.getElementById("b4"),


        buttonShop1 = document.getElementById("buttonShop1"),
        buttonShop2 = document.getElementById("buttonShop2"),
        buttonShop3 = document.getElementById("buttonShop3"),
        buttonShop4 = document.getElementById("buttonShop4"),

        buttonOptions1 = document.getElementById("buttonOptions1"),
        buttonOptions2 = document.getElementById("buttonOptions2"),
        buttonOptions3 = document.getElementById("buttonOptions3"),
        buttonOptions4 = document.getElementById("buttonOptions4"),

        buttonRestartIt = document.getElementById("buttonRestartIt"),


        logic,
        curUpgrade = 0,
        maxUpgrade = [];


    function initButtonWorker(world, status) {
        worldcode = world;
        logic = status;
        addListeners();
        maxUpgrade = [logic.getCostsHull(),

            logic.getCostsShield(),
            logic.getCostsRecovery(),

            logic.getCostsDamage(),
            logic.getCostsWeapon(),

            logic.getCostsSpeed(),
            logic.getCostsAcc(),


            logic.getCostsWireMe(),
            logic.getCostsWireEn()
        ];
    }

    function addListeners() {
        window.addEventListener("paused", pauseStateChanged);
        window.addEventListener("showDeadScreen", showDeadScreen);


        saveButton.addEventListener("click", saveButtonClicked);
        loadButton.addEventListener("click", loadButtonClicked);


        buttonOkay.addEventListener("click", startPageClick);
        buttonStart.addEventListener("click", startButtonClicked);
        buttonShop.addEventListener("click", openShop);
        buttonOptions.addEventListener("click", openOptions);

        buttonOptions1.addEventListener("click", goBackfromOptions);
        buttonOptions2.addEventListener("click", setFpsto30);
        buttonOptions3.addEventListener("click", setFpsto45);
        buttonOptions4.addEventListener("click", setFpsto60);

        buttonRestartIt.addEventListener("click", restartIt);


        buttonShop1.addEventListener("click", previousUpgrade);
        buttonShop2.addEventListener("click", nextUpgrade);
        buttonShop3.addEventListener("click", buyCurrentUpgade);
        buttonShop4.addEventListener("click", goBackfromShop);
    }

    function pauseStateChanged(event) {
        paused = event.detail;


    }


    function restartIt() {

        dispatchNewEvent("restartIt");

        worldcode.makeRestartPossible();

    }

    function gameLoadedChanged() {
        dispatchNewEvent("gameLoadingChanged", gameLoaded, paused, pausePressedBefore);
    }

    function dispatchNewEvent(eventName, detail, detail2, detail3) {
        var event = new CustomEvent(eventName, {
            "detail": detail,
            "detail2": detail2,
            "detail3": detail3
        });
        window.dispatchEvent(event);


    }

    function saveButtonClicked() {
        dispatchNewEvent("saveLocaly");


    }

    function loadButtonClicked() {
        dispatchNewEvent("loadLocaly");


    }



    function showDeadScreen() {



        gameLoaded = 2;
        gameLoadedChanged();
        paused = true;
        pausePressedBefore = false;



    }

    function setFpsto30() {
        worldcode.changeFps(30);
        logic.safeFpsOption(30);
    }

    function setFpsto45() {
        worldcode.changeFps(45);
        logic.safeFpsOption(45);
    }

    function setFpsto60() {
        worldcode.changeFps(60);
        logic.safeFpsOption(60);
    }

    function goBackfromOptions() {
        dispatchNewEvent("hideOptions");

    }

    function openOptions() {

        dispatchNewEvent("showOptions");



    }

    function buyCurrentUpgade() {


        dispatchNewEvent("upgradeShop", upgradeNumber);
        setShopContent(upgradeNumber);
    }

    function previousUpgrade() {
        if (upgradeNumber > 0) {
            upgradeNumber = upgradeNumber - 1;
            setShopContent(upgradeNumber);
        } else {
            upgradeNumber = shopDescriptions.length - 1;
            setShopContent(upgradeNumber);
        }
    }

    function nextUpgrade() {

        if (upgradeNumber < shopDescriptions.length - 1) {
            upgradeNumber = upgradeNumber + 1;
            setShopContent(upgradeNumber);
        } else {
            upgradeNumber = 0;
            setShopContent(upgradeNumber);
        }

    }

    function goBackfromShop() {
        dispatchNewEvent("hideShopScreen");

    }

    function setShopContent(index) {
        var upgrade, shopCostText;
        curUpgrade = logic.getLevels()[index] - 1;




        upgrade = "You have: " + curUpgrade + " from maximal: " + maxUpgrade[index].length + " possible upgrades       ";

        dispatchNewEvent("setShopImage", shopImages[index]);
        dispatchNewEvent("setShopUpgradeTimes", upgrade);
        dispatchNewEvent("setShopDescriptionText", shopDescriptions[index]);

        if (curUpgrade < maxUpgrade[index].length) {
            shopCostText = "Upgrade-Cost: " + makeDigitLong(maxUpgrade[index][curUpgrade]);
        } else {
            shopCostText = "Fully Upgraded";
        }
        dispatchNewEvent("setShopCostText", shopCostText);

    }


    function openShop() {

        descriptionText.className = "startScreenInvisible";
        setShopContent(0);
        shopScreen.className = "startScreen";
        upgradeNumber = 0;

    }

    function makeDigitLong(digit) {
        var longDigit;
        if (digit > 999) {
            longDigit = digit;
        } else {
            if (digit > 99) {
                longDigit = "0" + digit;
            } else {
                if (digit > 9) {
                    longDigit = "00" + digit;
                } else {

                    longDigit = "000" + digit;

                }
            }
        }
        return longDigit;
    }

    function startPageClick() {
        startScreenText.className = "startScreenInvisible";
        descriptionText.className = "startScreen";
        gameLoaded = gameLoaded + 1;
        gameLoadedChanged();
    }

    function readyLoaded() {


        dispatchNewEvent("setStartButtonContent", "Start Game");

        gameLoaded = gameLoaded + 1;
        gameLoadedChanged();
    }

    function startButtonClicked() {
        if (!restart) {
            if (gameLoaded >= 2) {

                dispatchNewEvent("setStartButtonContent", "Continue");
                gameLoaded = gameLoaded + 1;
                gameLoadedChanged();
                pause();
                if (pausePressedBefore) {

                    pausePressedBefore = false;
                }
            }
        } else {
            worldcode.unpauseGame();
            restart = false;
        }
    }

    function pause() {
        if (!pausePressedBefore) {
            if (!paused) {
                worldcode.pauseGame();

                dispatchNewEvent("showPauseScreen", "");
                paused = true;
            } else {

                dispatchNewEvent("hidePauseScreen", "");
                worldcode.unpauseGame();
                paused = false;
            }

            pausePressedBefore = true;

        }

    }





    that.showDeadScreen = showDeadScreen;
    that.readyLoaded = readyLoaded;
    that.initButtonWorker = initButtonWorker;

    return that;




};
